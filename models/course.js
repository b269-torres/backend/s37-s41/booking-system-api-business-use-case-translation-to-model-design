const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		
		required : [true, "First name is required!"]
	},
	description : {
		type : String,
		required : [true, "Description is required!"]
	},

	price : {
		type : Number,
		required :[true, "Price is required!"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},

	isActive : {
		type : Boolean,
		default : true
	},

	createdOn : {
		type : Date,
		default : new Date
	},
	
	enrollees : [
		{
			userId : {
				type : String,
				required : [true, "Course ID is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
		}
	]
});


module.exports = mongoose.model("Course", courseSchema);

// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


// This allows us to use all the routes defined in "userRoute.js"
const userRoute = require("./routes/userRoute");


// activity 
// This allows us to use all the routes defined in "userRoute.js"
const courseRoute = require("./routes/courseRoute");
// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json())
app.use(express.urlencoded({extended:true}));

// Allows all the user routes created in "userRoutes.js" file to use "/users" as route (resource)
// localhost:4000/users
app.use("/users", userRoute);


//activity 
// Allows all the user routes created in "userRoutes.js" file to use "/users" as route (resource)
app.use("/courses", courseRoute);


// Database Connection 
mongoose.connect("mongodb+srv://reybutchtorres:admin123@zuitt-bootcamp.p8hs8ti.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log('Now connected to the cloud database'));


// Server listening
// Will use the defined port number for the application whenever environment variable is available or used port 3000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`) )

// Activity
/*const userRoute = require("./routes/userRoute");
app.use("/course", userRoute);*/
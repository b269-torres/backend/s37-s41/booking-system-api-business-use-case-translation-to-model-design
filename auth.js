//JSON web token or jwt is a way of securely passing information from the server to the frontend or to other parts of server
const jwt = require("jsonwebtoken");

/*
ANATOMY
Header - type of token and signing algorithm
Payload - contains the claims (id, email, isAdmin)
Signature - generate by putting the encoded header, the encoded Payload, and applying the algorithm in the Header
*/
// Used in tha algorithm for encrypting our data which makes it difficult to decode the information without defined secrete keyword
const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// Generate a JSON web token, we are using the jwt's sign method
	return jwt.sign(data, secret, {});
};


// Token Verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) =>{
			if (err){
				return res.send({auth: "failed"});
			}else{
				next();
			}
		})
	} else{
		return res.send({auth: "failed"});
	};
};

// Token decryption
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null
			}else {
				return jwt.decode(token, {complete: true}).payload;
			};
		});
	}else{
		return null;
	};
};
const User = require("../models/User");
const Course = require("../models/Course");


const bcrypt = require("bcrypt");


const auth = require("../auth");
// Activity
/*const course = require("../models/course")*/
// Check if email already exist

/*
Business Logic:
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the Postman application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email: reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0 ) {
			return true;
			// No duplicate email found
			// The user is not yet registered in the database
		} else {
			return false
		}
	})
};


// Activity 
/*module.exports.checkNameExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return course.find({name: reqBody.name}).then(result => {
		
		if (result.length > 0 ) {
			return true;
			// No duplicate email found
			// The user is not yet registered in the database
		} else {
			return false
		}
	})
};*/


/*
BUSINESS LOGIC
1. Create a new User object using the mongoose model and the information from the request body
2. Make sure that the password is encrypted
3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// "bcrypt.hashSync" is a functon in the bcrypt library that used to generate hash value for a given input string synchronously
		// "reqBody.password" - input string that needs to be hashed
		// 10 is value provided as the number of "salt" rounds that the bcrypt algorythm will run in order to encrypt password
		password: bcrypt.hashSync(reqBody.password, 10)


});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});

};

// User authentication
/*
Business Logic:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
				// if password do not match
			}else {
				return false;
			};
		};
	});
};

/*
Business Logic:
1. Find the document in the database using the user's ID
2. Reassign the password of the returned document to an empty string
3. Return the result back to the frontend
*/

module.exports.getProfile=(data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// Authenticated user enrollment

/*
Business Logic:
1. Find the document in the database using the user's ID
2. Add the course ID to the user's enrollment array
3. Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (data) => {
	// Add course ID in the enrollments array of the user

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});
	// Add user ID in the enrollees array of the course 
	//isCourseUpdated = true
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error) => {
			if (error){
				return false;
			} else {
				return true;
			}
		})
	})
	if (isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	}
};
